#include<iostream>
#include<stdio.h>
#include<stdlib.h>
class Igrac {
	friend void postavljanje(Igrac* igraci);
	friend void pocetak(Igrac *igraci, int r, int s);
	friend void igra(Igrac*);
	friend bool gameover(Igrac* A, int i);
	char** privat;
public:
	int r, s;
	char** publik;
	Igrac() {}
	Igrac(int a, int b) :r(a), s(b) {
		privat = (char**)calloc(r, sizeof(char*));
		publik = (char**)calloc(r, sizeof(char*));
		for (int i = 0; i < r; i++) {
			privat[i] = (char*)calloc(s, sizeof(char));
			publik[i] = (char*)calloc(s, sizeof(char));
		}
	}
};

void CS() {
	for(int i=0;i<400;i++) std::cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << std::endl;
	return;
}

void ispis(char** Q, int r, int s) {
	//std::cout << "ispisujem: " << std::endl;
	//std::cout << "" << std::endl;
	std::cout << "   ";
	for (int x = 0; x < s; x++) {
		std::cout << "  " << (char)(x + 65) << " ";
	}
	for (int i = 0; i < r; i++) {
		std::cout << "" << std::endl;
		std::cout << "   ";
		for (int x = 0; x < s; x++) {
			std::cout << "----" ;
		}
		std::cout << "-";
		std::cout << "\n "<< i+1 <<" |";
		for (int j = 0; j < s; j++) {
			char iop;
			if (1 == Q[i][j]) iop = 'X';
			else iop = Q[i][j];
			std::cout << " "<< iop <<" |";

		}
	}
	std::cout << "" << std::endl;
	std::cout << "   ";
	for (int x = 0; x < s; x++) {
		std::cout << "----";
	}
	std::cout << "-";
	std::cout << "\n" << std::endl;
}

void postavljanje(Igrac *igraci) {
	int a1, a2, a3;
	std::cout << "Broj malih brodova(2): " << std::endl;
	std::cin >> a1;
	std::cout << "Broj srednjih brodova(3): " << std::endl;
	std::cin >> a2;
	std::cout << "Broj velikih brodova(4): " << std::endl;
	std::cin >> a3;
	for (int u = 0; u < 2; u++) {
		CS();
		std::cout << "Postavljanje ";
		!u ? std::cout << "prvog" : std::cout << "drugog";
		std::cout << " igraca: \n" << std::endl;
		char c = 0;
		char a[3];
		int b1, b2;
		for (int k = 0; k < 3; k++) {
			int q;
			switch (k) {
			case(0):q = a1; break;
			case(1):q = a2; break;
			case(2):q = a3; break;
			}
			for (int z = 0; z < q; z++) {
				std::cout<<""<<std::endl;
				ispis(igraci[u].privat, igraci[0].r, igraci[0].s);
				std::cout<<""<<std::endl;
				std::cout << "Postavljanje " << z + 1 << ". ";
				switch (k) {
				case 0: std::cout << "malog"; break;
				case 1: std::cout << "srednjeg"; break;
				case 2: std::cout << "velikog"; break;
				}

				std::cout << " broda, da li zelite da stoji vodoravno ili okomito? (V ili O)" << std::endl;
				do {
					std::cin >> c;
					//std::cout << "hey||" << c << "||" << std::endl;
				} while (c != 'V' && c != 'O' && c != 'v' && c != 'o');
				if (c >= 97 && 122 >= c)  c -= 32;
				do {
					std::cout << "Unesite oznaku polja ";
					if ('V' == c) std::cout << "lijevog ";
					if ('O' == c) std::cout << "gornjeg ";
					std::cout << "dijela broda" << std::endl;

					std::cin >> a[0];
					std::cin >> a[1];
					//fgets(a, 2, stdin);
					//std::cout << a << std::endl;

					if (a[0] >= 97 && 122 >= a[0]) a[0] -= 32;
					//a[0] -= 65;
					//a[1] -= 49;
					b2 = (int)a[0] - 65;
					b1 = (int)a[1] - 49;
					//std::cout << b1 << b2 << std::endl;
					if ('V' == c && b1 < igraci[0].r && (b2 + 1 + k) < igraci[0].s) break;
					if ('O' == c && b2 < igraci[0].s && (b1 + 1 + k) < igraci[0].r) break;
				} while (1); //treba rijesiti poduplavanje
				for (int t = 0; t < k + 2; t++) {
					igraci[u].privat[b1][b2] = 1;
					if ('V' == c) {
						if (igraci[u].privat[b1][b2 + t])igraci[u].privat[b1][b2 + t]++;
							igraci[u].privat[b1][b2 + t] = 1;
					}
					if ('O' == c) {
						if (igraci[u].privat[b1 + t][b2])igraci[u].privat[b1 + t][b2]++;
							igraci[u].privat[b1+t][b2] = 1;
					}
				}
			}
		}
	}
	CS();
	
	ispis(igraci[0].publik, igraci[0].r, igraci[0].s);
	return;
}

void pocetak(Igrac *A, int r, int s){
	A[0].r = r;
	A[0].s = s;
	A[1].r = r;
	A[1].s = s;

	A[0].privat = (char**)calloc(r, sizeof(char*));
	A[0].publik = (char**)calloc(r, sizeof(char*));
	A[1].privat = (char**)calloc(r, sizeof(char*));
	A[1].publik = (char**)calloc(r, sizeof(char*));
	for (int i = 0; i < r; i++) {
		A[0].privat[i] = (char*)calloc(s, sizeof(char));
		A[0].publik[i] = (char*)calloc(s, sizeof(char));
		A[1].privat[i] = (char*)calloc(s, sizeof(char));
		A[1].publik[i] = (char*)calloc(s, sizeof(char));	
	}
}

void igra(Igrac *igraci) {
	int b1, b2;
	char a[3];
	
	while (1) for (int u = 0; u < 2; u++) {
		do {
			do {
				std::cout << "Pogada ";
				u ? std::cout << "drugi" : std::cout << "prvi";
				std::cout << " igrac        ";
				std::cin >> a[0];
				std::cin >> a[1];
				std::cout << "\n" << std::endl;
				//fgets(a, 2, stdin);
				if (a[0] >= 97 && 122 >= a[0]) a[0] -= 32;
				b2 = (int)a[0] - 65;
				b1 = (int)a[1] - 49;				
			} while (b1 >= igraci[0].r && b2 >= igraci[0].s);
		} while (igraci[u].publik[b1][b2] > 0);
		igraci[u].publik[b1][b2] = 48;
		if (igraci[!u].privat[b1][b2] == 1) {
			igraci[u].publik[b1][b2] = 49;
			std::cout << "\nPogodak\n" << std::endl;
		}
		else {
			std::cout << "\nPromasaj\n" << std::endl;
		}
		std::cout << "\n\n\n" << std::endl;
		ispis(igraci[u].publik, igraci[0].r, igraci[0].s);
		if (gameover(igraci, u)) {
			std::cout << "Pobjedio je ";
			u ? std::cout << "drugi" : std::cout << "prvi";
			std::cout << "igrac" << std::endl;
			ispis(igraci[0].privat, igraci[0].r, igraci[0].s);
			std::cout<<"\n"<<std::endl;
			ispis(igraci[1].privat, igraci[0].r, igraci[0].s);
			
			exit(u);
		}
	}
}

bool gameover(Igrac *A, int z) { //dali je I-ti igrac pobjedio
	bool r=1;
	for (int i = 0; i < A[0].r; i++) {
		for (int j = 0; j < A[0].s; j++) {
			if (A[z].publik[i][j] == 0 && A[!z].privat[i][j] == 1) r = 0;
		}
	}
	return r;
}

int main() {
	int r, s;
	Igrac igraci[2];
	printf("Unesite broj redaka: ");
	std::cin >>r;
	printf("Unesite broj stupaca: ");
	std::cin >> s;
	pocetak(igraci, r, s);
	postavljanje(igraci);
	igra(igraci);
	return 0;
}

